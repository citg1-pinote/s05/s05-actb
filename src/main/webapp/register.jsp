<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
	<title>Registration Confirmation</title>
</head>
	<body>
		<div>
			<h1>Registration Confirmation</h1>
			<p>First Name: <%= session.getAttribute("fname") %></p>
			<p>Last Name: <%= session.getAttribute("lname") %></p>
			<p>Phone: <%= session.getAttribute("phone") %></p>
			<p>Email: <%= session.getAttribute("email") %></p>
			<p>App Discovery: <%= session.getAttribute("discovery") %></p>
			<p>Date of Birth: <%= session.getAttribute("birthdate") %></p>
			<p>User Type: <%= session.getAttribute("usertype") %></p>
			<p>Description: <%= session.getAttribute("profiledesc") %></p>
			<form action="login" method="post">
				<input type="submit">
			</form>	
			<form action="index.jsp">
				<input type="submit" value="Back">
			</form>	
		</div>	
	</body>
</html>
